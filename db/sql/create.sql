
CREATE TABLE Court(
    id_Court           INT            AUTO_INCREMENT,
    id_Court_Parent    INT            NOT NULL,
    Name               TEXT,
    Path               VARCHAR(10),
    CourtType          CHAR(1)        NOT NULL,
    Index              CHAR(5),
    PRIMARY KEY (id_Court)
)ENGINE=INNODB
;



CREATE TABLE Deal(
    id_Deal                  INT             AUTO_INCREMENT,
    id_Court                 INT,
    Number                   VARCHAR(50),
    StartYear                INT             NOT NULL,
    DebtorINN                CHAR(11),
    DebtorOGRN               CHAR(12),
    DebtorName               TEXT,
    Path                     VARCHAR(250),
    Closed                   BIT              DEFAULT 0 NOT NULL,
    ctb_publicDate           DATETIME,
    ctb_publicDate_loaded    DATETIME,
    last_load_time           DATETIME,
    PRIMARY KEY (id_Deal)
)ENGINE=INNODB
;



CREATE TABLE Document(
    id_Document     INT             AUTO_INCREMENT,
    id_Deal         INT             NOT NULL,
    id_Court        INT,
    Date            DATETIME,
    FileName        VARCHAR(250),
    DocumentType    CHAR(1),
    Downloaded      BIT              DEFAULT 0 NOT NULL,
    casebook_id     CHAR(10),
    PRIMARY KEY (id_Document)
)ENGINE=INNODB
;



CREATE UNIQUE INDEX byName ON Court(Name)
;
CREATE UNIQUE INDEX byIndex ON Court(Index)
;
CREATE INDEX byCourtType ON Court(CourtType)
;
CREATE INDEX refCourtParentCourt ON Court(id_Court_Parent)
;
CREATE UNIQUE INDEX byNumber ON Deal(Number)
;
CREATE INDEX byStartYear ON Deal(StartYear)
;
CREATE INDEX byDebtorINN ON Deal(DebtorINN)
;
CREATE INDEX byDebtorOGRN ON Deal(DebtorOGRN)
;
CREATE INDEX refDealCourt ON Deal(id_Court)
;
CREATE INDEX byDate ON Document(Date)
;
CREATE INDEX byDocumentType ON Document(DocumentType)
;
CREATE INDEX byDownloaded ON Document(Downloaded)
;
CREATE UNIQUE INDEX casebook_id ON Document(casebook_id)
;
CREATE INDEX refDocumentDeal ON Document(id_Deal)
;
CREATE INDEX refDocumentCourt ON Document(id_Court)
;
ALTER TABLE Court ADD CONSTRAINT refCourtParentCourt 
    FOREIGN KEY (id_Court_Parent)
    REFERENCES Court(id_Court)
;


ALTER TABLE Deal ADD CONSTRAINT refDealCourt 
    FOREIGN KEY (id_Court)
    REFERENCES Court(id_Court)
;


ALTER TABLE Document ADD CONSTRAINT refDocumentCourt 
    FOREIGN KEY (id_Court)
    REFERENCES Court(id_Court)
;

ALTER TABLE Document ADD CONSTRAINT refDocumentDeal 
    FOREIGN KEY (id_Deal)
    REFERENCES Deal(id_Deal)
;


