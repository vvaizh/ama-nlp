
if (WScript.FullName.indexOf("cscript.exe")==-1)
{
  WScript.Echo("This script may be runned only with CScript..  Try to run create_sql.bat")
  WScript.Quit(1)
}

// --------- settings -- {

var diagram_file_name=                            "model.dm1";
var physical_model_generation_settings_file_name= "mysql.gpo";
var ddl_generation_settings_file_name=            "mysql.ddo";
var physical_model_name=                          "mySQL_physical_model";

// --------- settings -- }

var fso = new ActiveXObject("Scripting.FileSystemObject");
var shell= new ActiveXObject("WScript.Shell");

var current_path= fso.GetFolder(".").Path;
var diagram_file_full_path= current_path + "\\model\\" + diagram_file_name;
var physical_model_generation_settings_file_full_path= 
  current_path + "\\model\\settings\\" + physical_model_generation_settings_file_name;
var ddl_generation_settings_file_path= 
  current_path + "\\model\\settings\\" + ddl_generation_settings_file_name;
var ddl_generation_settings_file_path_fixed= 
  current_path + "\\model\\settings\\fixed_" + ddl_generation_settings_file_name;
var CreationScriptFilePath= current_path + "\\sql\\create.wrong.sql";
var CreationScriptFilePathRight= current_path + "\\sql\\create.sql";

WScript.Echo("start ERStudio");

var er_studio= new ActiveXObject("ERStudio.Application");

function FindLogicalModel(diagram)
{
  WScript.Echo("FindLogicalModel");
  var models= diagram.Models;  
  var models_count= models.Count;
  WScript.Echo("  diagram.Models.Count=" + models_count);
  for (i= 1; i<=models_count; i++)
  {
    WScript.Echo("  get model " + i);
    var model= models(i);
    WScript.Echo("    model with name \"" + model.Name + "\"");
    if (!model.Logical)
    {
      WScript.Echo("    is not logical");
    }
    else
    {
      if (model.Name!="Main Model")
      {
        WScript.Echo("    is not Main Model");
      }
      //else
      {
        WScript.Echo("    is logical Main Model (return)");
        return model;
      }
    }
  }
  WScript.Echo("  return null");
  return null;
}

function process_ddo_file(file_name,new_file_name)
{
  var sprefix= "m_SingleSourceScriptFilePath=\"";
  var f= fso.OpenTextFile(file_name, 1);
  var new_f= fso.CreateTextFile(new_file_name, true);
  while (!f.AtEndOfStream)
  {
    var line= f.ReadLine();
    var i= line.indexOf(sprefix);
    if (i==-1)
    {
      new_f.WriteLine(line);
    }
    else
    {
      var i_rest= i + sprefix.length;
      var rest= line.substring(i_rest);
      var i_quote2= rest.indexOf("\"");
      new_f.WriteLine(line.substring(0,i_rest) + CreationScriptFilePath + rest.substring(i_quote2));
    }
  }
  f.Close();
  new_f.Close();  
  WScript.Echo("updated " + file_name);
}

function fix_create_sql_file()
{
  var sprefix= "CHAR(16)";
  var sprefix2= "TYPE=";
  var f= fso.OpenTextFile(CreationScriptFilePath, 1);
  var new_f= fso.CreateTextFile(CreationScriptFilePathRight);
  while (!f.AtEndOfStream)
  {
    var line= f.ReadLine();
    if (0!=line.indexOf("--"))
    {
      var i= line.indexOf(sprefix);
      if (-1 != i)
      {
        var i_rest= i + sprefix.length;
        var rest= line.substring(i_rest);
        new_f.WriteLine(line.substring(0,i) + "BINARY(16)" + rest);
      }
      else
      {
        i= line.indexOf(sprefix2);
        if (i==-1)
        {
          new_f.WriteLine(line);
        }
        else
        {
          var i_rest= i + sprefix2.length;
          var rest= line.substring(i_rest);
          new_f.WriteLine(line.substring(0,i) + "ENGINE=" + rest);
        }
      }
    }
  }
  f.Close();
  new_f.Close();
  fso.DeleteFile(CreationScriptFilePath);
}

try
{
  WScript.Echo("HideWindow");
  er_studio.HideWindow();

  WScript.Echo("open \"" + diagram_file_full_path + "\"");
  var diagram= er_studio.OpenFile(diagram_file_full_path);

  var models= diagram.Models;

  WScript.Echo("remove model \"" + physical_model_name + "\"");
  models.Remove(physical_model_name);

  var logical_model= FindLogicalModel(diagram);

  WScript.Echo("get PhysicalGenerationObject for logical model"); 
  var physical_generation= logical_model.PhysicalGenerationObject;
  if (physical_generation==null)
  {
    WScript.Echo("PhysicalGenerationObject is null"); 
  }

  WScript.Echo("Generate pgysical model from file \"" + physical_model_generation_settings_file_full_path + "\""); 
  physical_generation.GenPhysicalUsingFileBasedQuickLaunch(physical_model_generation_settings_file_full_path);

  WScript.Echo("Get generated physical model"); 
  var physical_model= diagram.Models(physical_model_name);
  if (physical_model==null)
  {
    WScript.Echo("physical_model is null"); 
  }

  WScript.Echo("get DDLGenerationObject for physical model"); 
  var ddl_generation= physical_model.DDLGenerationObject;
  if (ddl_generation==null)
  {
    WScript.Echo("DDLGenerationObject is null"); 
  }

  process_ddo_file(ddl_generation_settings_file_path,ddl_generation_settings_file_path_fixed);

  WScript.Echo("generate sql scripts using file \"" + ddl_generation_settings_file_path_fixed + "\""); 
  ddl_generation.GenDDLUsingFileBasedQuickLaunch(ddl_generation_settings_file_path_fixed);

  fso.DeleteFile(ddl_generation_settings_file_path_fixed);

  fix_create_sql_file();

  /*WScript.Echo("save diagram as \"" + diagram_file_full_path + "\"");
  diagram.SaveFile(diagram_file_full_path);*/
}
finally
{
  WScript.Echo("quit");
  er_studio.Quit();

  WScript.Echo("after quit sleep for second");
  WScript.Sleep(1000);
}




